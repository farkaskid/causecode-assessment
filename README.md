# Causecode Assessment #

This repo contains the project for the causecode assessment.

## Features Completed ##
* The Contact Domain class that contains 5 basic fields.
* The User domain class, this is main user class that will hold user contacts and groups.
* Secured User class with springsecuritycore as specified, needed only a single role though, no admin site.
* The frontend App is made using React.js.
* The CRUD of contacts.
* Contact Groups, add and remove contacts from groups.
* Create and Delete groups.

## Missing Features & Future Additions ##
* Oauth, tried but all the tutorial i found were using gsp views, could'nt reproduce the effect of <oauth: connect> tag
* A light theme.
* merge the signup and login forms with the mainpage as popups

## Extras ##
* Added a incremental search functionality that can search by any attribute.
* A small tutorial that guides on the usage and can be turned on/off.

## React Components ##
* All the components are made from scratch.
* Paid a lot of attention on Form and List components. They are highly configurable and programmable.

** Note: ** The App is deployed on Heroku and it shuts the app down if there is no client activity. So it is possible that you may encouter Application Error on the first visit, just wait a few minutes and reload.

[Here is the link](https://aqueous-beyond-88626.herokuapp.com)

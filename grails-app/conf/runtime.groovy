import org.scribe.builder.api.FacebookApi

oauth {
    providers {
        facebook {
            api = FacebookApi
            key = '395128557528191'
            secret = '66755781ad58d5bca563087c97583381'
            successUri = "${application.baseUrl}/hello/oauthFb"
			callback = "${application.baseUrl}/oauth/facebook/callback"
		}
    }
}

package helloworld
import helloworld.Role
import helloworld.AppUser
import helloworld.AppUserRole
import helloworld.Contact

class BootStrap {

    def init = {
        environments {
            production {
                def userRole = new Role(authority: 'ROLE_USER').save()
                println 'created role in production environment'
                AppUserRole.withSession {
        			it.flush()
        			it.clear()
        		}
            }

            development {
                def userRole = new Role(authority: 'ROLE_USER').save()
                println 'created role in development environment'
                AppUserRole.withSession {
        			it.flush()
        			it.clear()
        		}
            }
        }
		// def adminRole = new Role(authority: 'ROLE_ADMIN').save()
		// def testUser = new User(username: 'me', password: 'password', firstName: 'Siddharth', lastName: 'Shishulkar').save(flush: true)
		// UserRole.create testUser, userRole
		// for (def i = 0; i < 100 ; ++i) {
        //     testUser.addToContacts(firstName: 'Name ' + i, lastName: 'last ' + i, email: 'email ' + i, contactNumber: i, dob: 'date '+i, fav: 'false')
		// }
        // testUser.save(flush: true)


		// assert User.count() == 1
		// assert Role.count() == 2
		// assert UserRole.count() == 1

		// new Role(authority: 'ROLE_USER').save()
    }
    def destroy = {
    }
}

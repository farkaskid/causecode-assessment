package helloworld

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class AppUserRole implements Serializable {

	private static final long serialVersionUID = 1

	AppUser user
	Role role

	@Override
	boolean equals(other) {
		if (other instanceof AppUserRole) {
			other.userId == user?.id && other.roleId == role?.id
		}
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (user) builder.append(user.id)
		if (role) builder.append(role.id)
		builder.toHashCode()
	}

	static AppUserRole get(long userId, long roleId) {
		criteriaFor(userId, roleId).get()
	}

	static boolean exists(long userId, long roleId) {
		criteriaFor(userId, roleId).count()
	}

	private static DetachedCriteria criteriaFor(long userId, long roleId) {
		AppUserRole.where {
			user == AppUser.load(userId) &&
			role == Role.load(roleId)
		}
	}

	static AppUserRole create(AppUser user, Role role) {
		def instance = new AppUserRole(user: user, role: role)
		println(instance)
		instance.save()
		instance
	}

	static boolean remove(AppUser u, Role r) {
		if (u != null && r != null) {
			AppUserRole.where { user == u && role == r }.deleteAll()
		}
	}

	static int removeAll(AppUser u) {
		u == null ? 0 : AppUserRole.where { user == u }.deleteAll()
	}

	static int removeAll(Role r) {
		r == null ? 0 : AppUserRole.where { role == r }.deleteAll()
	}

	static constraints = {
		role validator: { Role r, AppUserRole ur ->
			if (ur.user?.id) {
				AppUserRole.withNewSession {
					if (AppUserRole.exists(ur.user.id, r.id)) {
						return ['userRole.exists']
					}
				}
			}
		}
	}

	static mapping = {
		id composite: ['user', 'role']
		version false
	}
}

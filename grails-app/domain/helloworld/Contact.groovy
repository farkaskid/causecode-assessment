package helloworld

class Contact {
    String firstName
    String lastName
    String email
    String contactNumber
    String dob
    String fav

	static belongsTo = [user: AppUser]

    static constraints = {
    }
}

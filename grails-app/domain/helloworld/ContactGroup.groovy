package helloworld

class ContactGroup {
    String name

    static hasMany = [members: Contact]

    static constraints = {
    }
}

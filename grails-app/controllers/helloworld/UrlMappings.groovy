package helloworld

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/favicon.ico"(controller: "hello", view: "/favicon")
        "/"(controller: "hello", view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}

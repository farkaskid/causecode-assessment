package helloworld
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import helloworld.AppUserRole
import helloworld.AppUser
import helloworld.Role
// import helloworld.Contact

class HelloController {

    def index() {}

    def registered() {
        def user = AppUser.findByUsername(request.JSON.username)
        if (user == null) {
            def u = new AppUser(request.JSON)
            u.save(failOnError: true)
            def userRole = Role.findByAuthority('ROLE_USER');
            AppUserRole.create u, userRole
            render 'true'
        } else
            render 'false'
    }

    def data() {
		if (loggedIn) {
            def user = AppUser.findByUsername(getPrincipal().username)
    		JSON.use('deep') {
    			render user as JSON
    	    }
		} else
            render 'false'
    }

    def isloggedin() {
        if (loggedIn)
            render 'true'
        else
            render 'false'
    }

    def favicon() {
        render(status: 200)
    }

    def oauthFb() {
        print ('aa gaya')
    }
}

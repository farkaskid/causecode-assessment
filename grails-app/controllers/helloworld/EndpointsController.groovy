package helloworld
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import helloworld.AppUser
import helloworld.Contact

@Secured('ROLE_USER')
class EndpointsController {

    def index() {}

    def fav() {
        def contact = Contact.findById(request.JSON.id)
        contact.fav = request.JSON.data
        contact.save(flush: true)
        render(status: 200)
    }

    def add() {
    	def user = AppUser.findByUsername(getPrincipal().username)
        user.addToContacts(request.JSON).save(flush: true)
        render Contact.findByEmail(request.JSON.email).id
    }

    def delete() {
        def longs = []
        def members
        def user = AppUser.findByUsername(getPrincipal().username)
        for (id in request.JSON)
            longs.push(new Long(id))
        for (group in user.groups) {
            members = []
            for (contact in group.members) {
                if (contact.id in longs)
                    members.push(contact)
            }
            for (contact in members)
                group.removeFromMembers(contact)
        }
        user.save(flush: true)
        Contact.where {
            id in longs
        }.deleteAll()
        render(status: 200)
    }

    def update() {
        def contact, user
        def ids = request.JSON.keySet() as List
        for (id in ids) {
            contact = Contact.findById(id)
            if (request.JSON[id].containsKey('firstName'))
                contact.firstName = request.JSON[id].firstName
            if (request.JSON[id].containsKey('lastName'))
                contact.lastName = request.JSON[id].lastName
            if (request.JSON[id].containsKey('email'))
                contact.email = request.JSON[id].email
            if (request.JSON[id].containsKey('contactNumber'))
                contact.contactNumber = request.JSON[id].contactNumber
            if (request.JSON[id].containsKey('dob'))
                contact.dob = request.JSON[id].dob
            contact.save(flush: true)
        }
        user = AppUser.findByUsername(getPrincipal().username)
        render(status: 200)
    }

    def creategroup() {
        def user = AppUser.findByUsername(getPrincipal().username)
        user.addToGroups(name: request.JSON.name).save(flush: true)
        def groups = AppUser.findByUsername(getPrincipal().username).groups
        for (group in groups) {
            if (group.name == request.JSON.name) {
                for(contact in request.JSON.contacts) {
                    group.addToMembers(Contact.findById(contact))
                }
                group.save(flush: true)
                break;
            }
        }
        render(status: 200)
    }

    def addtogroup() {
        def user = AppUser.findByUsername(getPrincipal().username)
        for (group in user.groups) {
            if (group.name == request.JSON.name) {
                for(contact in request.JSON.contacts) {
                    group.addToMembers(Contact.findById(contact))
                }
                group.save(flush: true)
                break
            }
        }
        render(status: 200)
    }

    def deletefromgroup() {
        def user = AppUser.findByUsername(getPrincipal().username)
        for (group in user.groups) {
            if (group.name == request.JSON.name) {
                for(contact in request.JSON.contacts) {
                    group.removeFromMembers(Contact.findById(contact))
                }
                group.save(flush: true)
                break
            }
        }
        render(status: 200)
    }

    def deletegroup() {
        def user = AppUser.findByUsername(getPrincipal().username)
        def toDelete
        for (group in user.groups) {
            if (group.name == request.JSON.name) {
                toDelete = group
                break
            }
        }
        user.removeFromGroups(toDelete)
        toDelete.delete()
        user.save(flush: true)
        render(status: 200)
    }
}

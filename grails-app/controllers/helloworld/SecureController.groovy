package helloworld
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.*

@Secured('ROLE_USER')
class SecureController {
    def index() {
    	def user = AppUser.findByUsername(params['username'])
    	user.contacts as JSON
    }
}

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Welcome...!</title>
	<asset:stylesheet href="bootstrap.css"/>
	<asset:stylesheet href="app.css"/>
	<asset:javascript src='jquery-2.2.0.min.js' />
	<asset:javascript src='bootstrap.js' />
</head>
<body>
	<!-- <div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8&appId=395128557528191";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> -->
	<div class="container" id="app"></div>
	<!-- <a href="/oauth/facebook/authenticate?redirectUrl=/hello/oauthFb">facebook</a> -->
	<!-- <oauth:connect provider="facebook" id="facebook-connect-link">Connect to Facebook</oauth:connect> -->
	<asset:javascript src="index.bundle.js"/>
</body>
</html>

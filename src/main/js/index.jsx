import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import Home from './Pages/home.jsx';
import Userpage from './Pages/user.jsx';

class Layout extends React.Component {

	navigate() {
		this.props.router.push('userpage');
	}

	render() {
		return (
			<div>
				{this.props.children}
			</div>
		);
	}
}

ReactDOM.render(
	<Router history={hashHistory}>
		<Route path="/" component={Layout}>
			<IndexRoute component={Home}></IndexRoute>
			<Route path="userpage/" component={Userpage}></Route>
		</Route>
	</Router>
,
document.getElementById('app'));

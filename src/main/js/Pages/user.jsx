import React from 'react';
import List from './components/list.jsx';
import Form from './components/Form.jsx';
import SnackBar from './components/Snackbar.jsx';
import axios from 'axios';
import val from 'validator';
import messages from './components/messages';

export default class Userpage extends React.Component{

	constructor(props) {
		super(props);
		this.selected = new Set();
		this.viewSets = [new Set(), new Set()];
		this.updated = {};
		this.store = [];
		this.groups = [];
		this.currentView = new Set();
		this.state = {
			tabActive: 0,
			count: 0,
			deleteActive: false,
			updateActive: false,
		};
		this.formData = {
			'position': 'container-fluid',
			'heading': 'Add New Contact',
			'label': ['First Name', 'Last Name', 'Email', 'Contact Number', 'DOB'],
			'type': ['text', 'text', 'email', 'text', 'date'],
			'validate': [(data) => data.length != 0, (data) => data.length != 0, this.isEmail.bind(this), (data) => val.isNumeric(data) && data.length >= 10, (data) => data.length == 10],
			'buttonText': 'Save'
		};
		this.formData1 = {
			'position': 'container-fluid',
			'heading': 'Give Group Name',
			'label': ['Unique Group Name'],
			'type': ['text'],
			'validate': [this.isGroupName.bind(this)],
			'buttonText': 'Go'
		}
	}

	isGroupName(name) {
		let groups = this.groups.map(group => group.name);
		return !groups.includes(name);
	}

	isEmail(name) {
		let mails = this.store.map(contact => contact.email);
		return (!mails.includes(name)) && val.isEmail(name);
	}

	enableSearch(e) {
		let searchBar = e.target.parentNode.nextSibling.firstChild;
		searchBar.disabled = false;
	}

	toCamel(s) {
		let res = '', word = false;
		for (let i = 0; i < s.length; ++i) {
			if (s[i] == ' ') {
				word = true;
			}

			else if (s[i] == s[i].toUpperCase()) {
				if (word) {
					res += s[i]
					word = false;
				}
				else {
					res += s[i].toLowerCase();
				}
			}

			else if (s[i] == s[i].toLowerCase()){
				if (word) {
					res += s[i].toUpperCase();
					word = false;
				}
				else {
					res += s[i];
				}
			}

			else {}
		}
		return res;
	}

	searchHandler(e) {
		let value = e.target.value, field = e.target.parentNode.previousSibling.firstChild.value;
		this.search(this.toCamel(field), value);
	}

	isMatched(value, regex) {
		let pattern = new RegExp('^' + regex.toLowerCase());
		return pattern.test(value.toLowerCase());
	}

	populateView(store, tab) {
		switch(tab.toString()) {
			case '0': {
				this.viewSets[0] = new Set(store);
				break;
			}

			case '1': {
				let res = new Set();
				for (let i = 0; i < store.length; i++) {
					if (store[i]['fav'] == 'true')
						res.add(store[i]);
				}
				this.viewSets[1] = res;
				break;
			}

			default: {
				let ids = new Set(this.groups[tab - 2].members.map(contact => contact.id)), contacts = [];
				for (let contact of this.store) {
					if (ids.has(contact.id))
						contacts.push(contact);
				}
				this.viewSets[tab] = new Set(contacts);
				break;
			}
		}
	}

	handleTab(tab) {
		this.populateView(this.store, tab);
		this.currentView = this.viewSets[tab];
		this.setState({ tabActive: tab });
	}

	search(field, value) {
		let data = Array.from(this.currentView), matched = [];
		for (let i in data) {
			if (this.isMatched(data[i][field], value))
				matched.push(data[i])
		}
		this.viewSets[this.state.tabActive] = new Set(matched);
		this.setState({ count: matched.length });
	}

	onBlurHandler(e) {
		// this.all = this.store;
		this.setState({ searched: true });
	}

	componentWillMount() {
		axios({
				method: 'post',
				url: '/hello/data',
			})
			.then((response) => {
				if (response.status == 200) {
					if (response.data) {
						this.store = response.data.contacts;
						this.groups = response.data.groups;
						for (let i = 0; i < this.groups.length; ++i) this.viewSets.push(new Set());
						this.populateView(this.store, this.state.tabActive);
						this.currentView = this.viewSets[this.state.tabActive];
						this.setState({
							tutorial: this.store.length == 0,
							firstName: response.data.firstName,
							count: response.data.contacts.length
						});
					} else {
						this.props.router.replace('/');
					}
				}
			})
			.catch(function (error) {
			    console.log(error);
			});
	}

	addUserHandler(data, buttonEvent, clearForm) {
		data['fav'] = 'false';
		axios({
				method: 'post',
				url: '/endpoints/add',
				data: data,
			})
			.then((response) => {
				if(response.status == 200) {
					data['id'] = response.data;
					this.store.push(data);
					this.viewSets[0].add(data);
					this.setState({ count: this.state.count + 1 });
					clearForm();
				}
			})
			.catch(function (error) {
			    console.log(error);
			});
		$('#myModal').modal('hide');
	}

	gatherSelected(tileData, value, tile) {
		if (value == 1) {
			this.selected.add(tileData);
		}
		else {
			this.selected.delete(tileData);
		}
		this.setState({deleteActive: this.selected.size != 0})
	}

	gatherUpdated(id, data) {
		this.updated[id] = data;
		this.setState({ updateActive: Object.keys(this.updated).length != 0})
	}

	gatherFav(id, data) {
		id['fav'] = data;
		for (let i = 0; i < this.store.length; i++) {
			if (id['id'] == this.store[i]['id']) {
				this.store[i]['fav'] = data;
			}
		}
		if (data == 'true') {
			this.viewSets[1].add(id);
		} else {
			this.viewSets[1].delete(id);
		}
		axios({
				method: 'post',
				url: '/endpoints/fav',
				data: {
					id: id['id'],
					data: data
				},
			})
			.then((response) => {
				if(response.status == 200) {
				}
			})
			.catch(function (error) {
			    console.log(error);
			});
	}

	deleteUserHandlerGroup() {
		if (this.state.deleteActive && this.state.tabActive >= 2) {
			axios({
					method: 'post',
					url: '/endpoints/deletefromgroup',
					data: {
						name: this.groups[this.state.tabActive - 2].name,
						contacts: Array.from(this.selected)
					},
				})
				.then((response) => {
					if(response.status == 200) {
						let newData = [];
						for (let contact of this.groups[this.state.tabActive - 2].members) {
							if (!this.selected.has(contact.id))
								newData.push(contact);
						}
						this.groups[this.state.tabActive - 2].members = newData;
						this.populateView(this.store, this.state.tabActive);
						this.currentView = this.viewSets[this.state.tabActive];
						this.selected.clear();
						this.setState({
							degrouped: true,
							count: this.viewSets[this.state.tabActive].size,
							deleteActive: false
						});
					}
				})
				.catch(function (error) {
				    console.log(error);
				});
		}
	}

	deleteUserHandler() {
		if (this.state.deleteActive) {
			axios({
					method: 'post',
					url: '/endpoints/delete',
					data: Array.from(this.selected),
				})
				.then((response) => {
					if(response.status == 200) {
						let newData = [], old = this.store;
						for (let contact of old) {
							if (!this.selected.has(contact.id))
								newData.push(contact)
						}
						this.store = newData;
						for (let group of this.groups) {
							old = group.members, newData = [];
							for (let contact of old) {
								if (!this.selected.has(contact.id))
									newData.push(contact)
							}
							group.members = newData;
						}
						this.populateView(this.store, this.state.tabActive);
						this.currentView = this.viewSets[this.state.tabActive];
						this.selected.clear();
						this.setState({
							count: this.viewSets[this.state.tabActive].size,
							deleteActive: false
						});
					}
				})
				.catch(function (error) {
				    console.log(error);
				});
		}
	}

	updateUserHandler() {
		if (this.state.updateActive && Object.keys(this.updated).length != 0) {
			axios({
					method: 'post',
					url: '/endpoints/update',
					data: this.updated,
				})
				.then((response) => {
					if(response.status == 200) {
						let index;
						for (let id of Object.keys(this.updated)) {
							index = this.store.findIndex(contact => contact.id == id);
							for (let property of Object.keys(this.updated[id]))
								this.store[index][property] = this.updated[id][property];
						}
						switch(parseInt(this.state.tabActive)) {
							case 0: {
								this.currentView = new Set(this.store);
								break;
							}

							case 1: {
								let favs = []
								for (let contact of this.store) {
									if (contact['fav'] == 'true')
										favs.push(contact);
								}
								this.currentView = new Set(favs);
								break;
							}

							default: {
								let group = new Set(this.groups[this.state.tabActive - 2].members), contacts = new Set();
								for (let contact of this.store) {
									if (group.has(contact))
										contacts.add(contact);
								}
								this.currentView = new Set(contacts);
								break;
							}
						}
						this.updated = {};
						this.setState({
							updated: true,
							updateActive: false
						});
					}
				})
				.catch(function (error) {
				    console.log(error);
				});
		}
	}

	outdate() {
		this.setState({ updateActive: true });
	}

	logout() {
		axios({
				method: 'get',
				url: '/logoff',
				headers: {'X-Requested-With': 'XMLHttpRequest'}
			})
			.then((response) => {
			  	this.props.router.replace('/');
			})
			.catch(function (error) {
			    console.log(error);
			});
	}

	addToGroup(e) {
		let name = e.target.textContent, index = this.groups.findIndex(group => group.name == name);
		axios({
			'method': 'post',
			'url': 'endpoints/addtogroup',
			'data': {
				name: name,
				contacts: Array.from(this.selected)
			}
		})
		.then((response) => {
			if (response.status == 200) {
				let members = [], member_ids = new Set(this.groups[index].members.map(contact => contact.id));
				for (let contact of this.store) {
					if (this.selected.has(contact.id) && !member_ids.has(contact.id)) {
						members.push(contact);
					}
				}
				this.groups[index] = {
					name: name,
					members: this.groups[index].members.concat(members)
				}
				this.populateView(this.store, this.groups.length + 1);
				this.currentView = this.viewSets[this.groups.length + 1];
				this.selected.clear();
				this.setState({
					tabActive: this.groups.length + 1,
					deleteActive: false
				})
			}
		})
		.catch((error) => {console.log(error)});
	}

	deleteGroup(e) {
		if (this.state.tabActive >= 2) {
			let name = this.groups[this.state.tabActive - 2].name;
			axios({
				'method': 'post',
				'url': '/endpoints/deletegroup',
				'data': {
					'name': name
				}
			})
			.then((response) => {
				if (response.status == 200) {
					let groups = [];
					this.groups.forEach((group) => {
						if (group.name != name) {
							groups.push(group);
						}
					})
					this.groups = groups;
					this.populateView(this.store, 0);
					this.currentView = this.viewSets[0];
					this.selected.clear();
					this.setState({
						tabActive: 0,
						deleteActive: false
					})
				}
			})
		}

	}

	createGroup(data, buttonEvent, clearForm) {
		$('#myModal1').modal('hide');
		clearForm();
		axios({
			'method': 'post',
			'url': 'endpoints/creategroup',
			'data': {
				name: data['uniqueGroupName'],
				contacts: Array.from(this.selected)
			}
		})
		.then((response) => {
			if (response.status == 200) {
				let members = [];
				for (let contact of this.store) {
					if (this.selected.has(contact.id)) {
						members.push(contact);
					}
				}
				this.groups.push({
					name: data['uniqueGroupName'],
					members: members
				});
				this.viewSets.push(new Set());
				this.populateView(this.store, this.groups.length + 1);
				this.currentView = this.viewSets[this.groups.length + 1];
				this.selected.clear();
				this.setState({
					grouped: true,
					tabActive: this.groups.length + 1,
					deleteActive: false
				})
			}
		})
		.catch((error) => {console.log(error)});
	}

	render() {
		let list = {
			active: this.state.tabActive,
			label: ['All', 'Favourites'].concat(this.groups.map(group => group.name)),
			data: this.viewSets.map(set => Array.from(set)),
			checks: [(data) => data.length != 0, (data) => data.length != 0, this.isEmail.bind(this), (data) => val.isNumeric(data) && data.length >= 10, (data) => data.length == 10]
		};
		let groups = this.groups.map(group => <li key={group.name}><a onClick={this.addToGroup.bind(this)}>{group.name.length <= 10 ? group.name : `${group.name.slice(0, 10)}...`}</a></li>);
		let deleteBar;
		if (!this.state.deleteActive && this.state.tabActive < 2) {
			deleteBar = (
				<li className='dropdown disabled'>
				  <a className='dropdown-toggle disabled' data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Delete<span className="caret"></span></a>
				</li>
			);
		} else if (!this.state.deleteActive && this.state.tabActive >= 2) {
			deleteBar = (
				<li className='dropdown'>
				  <a className='dropdown-toggle'  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Delete<span className="caret"></span></a>
				  <ul className="dropdown-menu">
					<li><a onClick={this.deleteGroup.bind(this)}>Current Group</a></li>
				  </ul>
				</li>
			);
		} else if (this.state.deleteActive && this.state.tabActive < 2) {
			deleteBar = (
				<li className='dropdown'>
				  <a className='dropdown-toggle' data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Delete<span className="caret"></span></a>
				  <ul className="dropdown-menu">
					<li><a onClick={this.deleteUserHandler.bind(this)}>Contacts From Everywhere</a></li>
				  </ul>
				</li>
			);
		} else {
			deleteBar = (
				<li className='dropdown'>
				  <a className='dropdown-toggle' data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Delete<span className="caret"></span></a>
				  <ul className="dropdown-menu">
				  	<li><a onClick={this.deleteUserHandlerGroup.bind(this)}>Contacts From Current Group</a></li>
					<li><a onClick={this.deleteUserHandler.bind(this)}>Contacts From Everywhere</a></li>
					<li role="separator" className="divider"></li>
					<li><a onClick={this.deleteGroup.bind(this)}>Current Group</a></li>
				  </ul>
				</li>
			);
		}
		return (
			<div>
				<nav className="navbar navbar-inverse">
				  <div className="container-fluid">
				    <div className="navbar-header">
				      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span className="sr-only">Toggle navigation</span>
				        <span className="icon-bar"></span>
				        <span className="icon-bar"></span>
				        <span className="icon-bar"></span>
				      </button>
				      <a className="navbar-brand">{this.state.firstName}</a>
				    </div>

				    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul className="nav navbar-nav">
				        <li><a data-toggle="modal" data-target="#myModal">Add Contact</a></li>

				        <div id="myModal1" className="modal fade" role="dialog">
						  <div className="modal-dialog">

						    <div className="modal-content">
						      <div className="modal-body">
						        <Form handler={this.createGroup.bind(this)} data={this.formData1}></Form>
						      </div>
						    </div>

						  </div>
						</div>

						<div id="myModal" className="modal fade" role="dialog">
						  <div className="modal-dialog">

						    <div className="modal-content">
						      <div className="modal-body">
						        <Form handler={this.addUserHandler.bind(this)} data={this.formData}></Form>
						      </div>
						    </div>

						  </div>
						</div>

						{deleteBar}

				        <li className={this.state.updateActive && Object.keys(this.updated).length != 0 ? '' : 'disabled'}><a className={this.state.updateActive && Object.keys(this.updated).length != 0 ? '' : 'disabled'} onClick={this.updateUserHandler.bind(this)}>Update</a></li>

				        <li className={`dropdown ${this.state.deleteActive ? '' : 'disabled'}`}>
				          <a className={`dropdown-toggle ${this.state.deleteActive ? '' : 'disabled'}`} data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Add To Group <span className="caret"></span></a>
				          <ul className="dropdown-menu">
				            { groups }
				            {this.groups.length ? <li role="separator" className="divider"></li> : ''}
				            <li><a data-toggle="modal" data-target="#myModal1">New Group</a></li>
				          </ul>
				        </li>
				      </ul>
				      <form className="navbar-form navbar-left">
				      	<div className="form-group">
						  <select onClick={this.enableSearch.bind(this)} defaultValue='Search By' className="form-control searchForm">
						    <option disabled>Search By</option>
						    <option>First Name</option>
						    <option>Last Name</option>
						    <option>Email</option>
						    <option>Contact Number</option>
						  </select>
						</div>
				        <div className="form-group">
				          <input onBlur={this.onBlurHandler.bind(this)} onChange={this.searchHandler.bind(this)} type="text" className="form-control searchForm" placeholder="Search" disabled/>
				        </div>
				      </form>
					  <ul className="nav navbar-nav navbar-right">
				        <li><button onClick={this.logout.bind(this)} className='btn btn-danger navbar-btn'>Log Out</button></li>
				      </ul>
					  <ul className="nav navbar-nav navbar-right">
				        <li><button onClick={() => {this.setState({tutorial: !this.state.tutorial})}} className={`btn navbar-btn ${this.state.tutorial ? 'btn-primary' : ''}`}>Tutorial</button></li>
				      </ul>
				    </div>
				  </div>
				</nav>
				<div className='jumbotron'>
					<List handleTab={this.handleTab.bind(this)} deleteActive={this.state.deleteActive} updateActive={this.state.updateActive} highlightHandler={this.outdate.bind(this)} updateHandler={this.gatherUpdated.bind(this)} selectHandler={this.gatherSelected.bind(this)} favHandler={this.gatherFav.bind(this)} list={list} />
				</div>
				<SnackBar show={ this.state.tutorial && this.currentView.size == 0 && !this.state.degrouped ? 'show' : ''} message={messages[0]}/>
				<SnackBar show={ this.state.tutorial && this.currentView.size == 1 && this.selected.size == 0 && !this.state.updated && !this.state.searched ? 'show' : ''} message={messages[1]}/>
				<SnackBar show={ this.state.tutorial && this.currentView.size == 1 && this.selected.size == 1 && !this.state.updated && !this.state.searched ? 'show' : ''} message={messages[2]}/>
				<SnackBar show={ this.state.tutorial && this.currentView.size == 2 && !this.state.updateActive && !this.state.updated && !this.state.searched ? 'show' : ''} message={messages[3]}/>
				<SnackBar show={ this.state.tutorial && this.currentView.size >= 2 && this.state.updateActive && Object.keys(this.updated).length == 0 && !this.state.updated ? 'show' : ''} message={messages[4]}/>
				<SnackBar show={ this.state.tutorial && this.currentView.size >= 2 && this.state.updateActive && Object.keys(this.updated).length != 0 && !this.state.updated && !this.state.searched ? 'show' : ''} message={messages[5]}/>
				<SnackBar show={ this.state.tutorial && this.currentView.size >= 2 && !this.state.updateActive && this.state.updated && !this.state.searched && !this.state.grouped ? 'show' : ''} message={messages[6]}/>
				<SnackBar show={ this.state.tutorial && !this.state.updateActive && this.state.updated && !this.state.searched && this.state.grouped && !this.state.degrouped ? 'show' : ''} message={messages[7]}/>
				<SnackBar show={ this.state.tutorial &&  !this.state.searched && this.state.updated && this.state.grouped && this.state.degrouped ? 'show' : ''} message={messages[8]}/>
				<SnackBar show={ this.state.tutorial &&  this.state.searched && this.state.count != this.store.length ? 'show' : ''} message={messages[9]}/>
				<SnackBar show={ this.state.tutorial &&  this.state.count == this.store.length && this.state.searched ? 'show' : ''} message={messages[10]}/>
			</div>
		)
	}
}

import React from 'react';
import Form from './components/Form.jsx';
import { Link } from 'react-router';
import axios from 'axios';
import querystring from 'querystring';
import SnackBar from './components/Snackbar.jsx';

let emptycheck = (data) => data.length != 0

let passwordCheck = (data) => data.length >= 8

let logInForm = {
	'heading': 'Log In',
	'position': 'col-lg-5 col-md-5 col-sm-12 col-xs-12 loginForm',
	'label': ['Username', 'Password'],
	'type': ['text', 'password'],
	'validate': [emptycheck, passwordCheck],
	'buttonText': 'Go'
};

let signUpForm = {
	'heading': 'Sign Up',
	'position': 'col-lg-6 col-md-6 col-sm-12 col-xs-12 pull-right signupForm',
	'label': ['Username', 'Password', 'First Name', 'Last Name'],
	'type': ['text', 'password', 'text', 'text'],
	'validate': [emptycheck, passwordCheck, emptycheck, emptycheck],
	'buttonText': 'Get Started'
};


export default class Home extends React.Component{

	constructor(props) {
		super(props);
		this.state = {};
	}

	componentWillMount() {
		axios({
			url: '/hello/isloggedin',
			method: 'post'
		})
		.then((response) => {
			if (response.status == 200) {
				if (response.data)
					this.props.router.replace('userpage/');
			}
		})
		.catch((err) => {
			alert('slow/no internet connectivity');
		})
	}

	signupHandler(data, buttonEvent, clearForm) {
		let btn = buttonEvent.target;
		btn.disabled = true;
		axios({
				method: 'post',
				url: '/hello/registered',
				data: data,
			})
			.then((response) => {
				if(response.status == 200) {
					if (response.data) {
						this.setState({signedUp: true});
						clearForm();
					} else {
						this.setState({usernameExists: true});
						clearForm();
					}
					btn.disabled = false;
				}
			})
			.catch(function (error) {
				btn.disabled = false;
			});
	}

	loginHandler(data, buttonEvent, clearForm) {
		let btn = buttonEvent.target;
		buttonEvent.target.disabled = true;
		axios({
				method: 'post',
				url: '/login/authenticate',
				data: querystring.stringify(data),
				headers: {'X-Requested-With': 'XMLHttpRequest'}
			})
			.then((response) => {
				if (response.data.success) {
			  		this.props.router.replace('userpage/');
			  		clearForm();
				}
				else {
					this.setState({wrongPassword: true});
					btn.disabled = false;
				}
			})
			.catch(function (error) {
			    btn.disabled = false;
			});
	}

	render() {
		return (
			<div className='row'>
				<Form handler={this.loginHandler.bind(this)} data={logInForm} />
				<Form handler={this.signupHandler.bind(this)} data={signUpForm} />
				<SnackBar show={ this.state.signedUp ? 'show' : ''} message="Registered Successfully"/>
				<SnackBar show={ this.state.usernameExists ? 'show' : ''} message="Sorry, this username is already taken"/>
				<SnackBar show={ this.state.wrongPassword ? 'show' : ''} message="Incorrect Password or Username"/>
			</div>
		);
	}
}

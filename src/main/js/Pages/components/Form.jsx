// generate the layout of a form with Standard Bootstrap classes(no theme) given an object with two arrays:
// 1. type 2. label
// type array has the type of each element.
// label array's element is either a string or an array of string depending upon the type of InputField	

import React from 'react';
import InputField from './InputField.jsx';

export default class Form extends React.Component{

	constructor() {
		super();
		this.data = {};
		this.inputs = {};
		this.status = {};
		this.state = {
			modified: false,
			filled: false
		};
	}

	formCheck() {
		let correctFields = 0;
		for (let field in this.status) {
			if(this.status[field]) {
				correctFields++;
			}
			else {
				break;
			}
		}
		return correctFields == this.props.data.type.length;
	}

	toCamel(s) {
		let res = '', word = false;
		for (let i = 0; i < s.length; ++i) {
			if (s[i] == ' ') {
				word = true;
			}

			else if (s[i] == s[i].toUpperCase()) {
				if (word) {
					res += s[i]
					word = false;
				}
				else {
					res += s[i].toLowerCase();
				}
			}

			else if (s[i] == s[i].toLowerCase()){
				if (word) {
					res += s[i].toUpperCase();
					word = false;
				}
				else {
					res += s[i];
				}
			}

			else {}
		}
		return res;
	}

	updateState(label, data, status, element) {
		this.inputs[this.toCamel(label)] = element;
		this.data[this.toCamel(label)] = data;
		this.status[this.toCamel(label)] = status;
		let state = !this.state.modified;
		if (this.formCheck())
			this.setState({
				status: state,
				filled: true
			});
		else
			this.setState({
				status: state,
				filled: false
			});
	}

	clearForm() {
		let state = !this.state.modified;
		for (let input in this.inputs) {
			this.inputs[input].value = null;
		}
		this.data = {};
		this.inputs = {};
		this.status = {};
		this.setState({
			modified: state,
			filled: false
		});
	}

	clickHandler(e) {
		if (this.formCheck()) {
			this.props.handler(this.data, e, this.clearForm.bind(this));
		}
	}

	render() {
		let data = this.props.data, list = [];
		for (let i = 0; i < data.type.length; i++) {
			let truthValue;
			switch(this.status[this.toCamel(data.label[i])]) {
				case true: truthValue = true;
						   break;

				case false: truthValue = false;
							break;

				default: 
			}
			list.push(<InputField truthValue={truthValue} check={data.validate[i].bind(this)} key={i.toString()} updateState={this.updateState.bind(this)} type={data.type[i]} label={data.label[i]} />);
		}
		return (
			<div className={data.position}>
				<h3 className='formHeading'>
					{data.heading}
				</h3>
				<hr />
				{list}
				<div className='pull-right'><button onClick={this.clickHandler.bind(this)} className={this.state.filled ? 'btn formBtn-success' : 'btn formBtn'} id='submit'>{data.buttonText}</button></div>
			</div>
		);
	}
}
import React from 'react';
export default class InputField extends React.Component{

	handleChange(e) {
		let element = e.target;
		switch(element.getAttribute('type')) {
			default: {
				let truthValue = this.props.check(element.value.trim());
				this.props.updateState(element.getAttribute('placeholder'), element.value.trim(), truthValue, element);
			}
		}
	}

	render() {
		switch(this.props.type) {
			default: {
				let isCorrect = '', icon;
				switch(this.props.truthValue) {
					case true: isCorrect = 'has-success';
							icon = <span className="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>;
							break;

					case false: isCorrect = 'has-warning';
							icon = <span className="glyphicon glyphicon-warning-sign form-control-feedback" aria-hidden="true"></span>
							break

					default: isCorrect = '';
							  icon = '';
				}
				return (
					<div className={'form-group has-feedback ' + isCorrect}>
						<input onChange={this.handleChange.bind(this)} type={this.props.type} className='form-control my-form-control' placeholder={this.props.label}/>
						{icon}
					</div>
				);
			}
		}
	}
}

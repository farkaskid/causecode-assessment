import React from 'react';
import Form from './Form.jsx';

export default class Modal extends React.Component{

	handler(data, buttonEvent, clearForm) {
		this.props.handler(data, buttonEvent, clearForm);
	}

	render() {
		return (
			<div className={this.props.location}>
				<a data-toggle="modal" data-target="#myModal">{this.props.buttonText}</a>

				<div id="myModal" className="modal fade" role="dialog">
				  <div className="modal-dialog">

				    <div className="modal-content">
				      <div className="modal-body">
				        <Form handler={this.handler.bind(this)} data={this.props.formData}></Form>
				      </div>
				    </div>

				  </div>
				</div>
			</div>
		);
	}	
}
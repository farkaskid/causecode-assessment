import React from 'react';

export default class ListItem extends React.Component{

	constructor(props) {
		super(props);
		this.updated = {};
		this.fields = new Set();
	}

	onUnselectHandler(e) {
		this.props.onSelectHandler(this.props.item.id, 0, e.target);
	}

	onSelectHandler(e) {
		this.props.onSelectHandler(this.props.item.id, 1, e.target);
	}

	onHighlightHandler(e) {
		this.props.onHighlightHandler(this.props.item.id, 2, e.target);
	}

	onUnhighlightHandler(e) {
		this.props.onHighlightHandler(this.props.item.id, 0, e.target);
	}

	normalizeHandler(e) {
		this.props.onHighlightHandler(this.props.item.id, 0, e.target);
	}

	blurHandler(e) {
		if (this.props.stage == 2) {
			let fields = this.fields;
			for (let field of fields) {
				let id;
				switch (field.id) {
					case 'firstName':
						id = 0;
						break;
					case 'lastName':
						id = 1;
						break;
					case 'email':
						id = 2;
						break;
					case 'contactNumber':
						id = 3;
						break;
					default:
						id = 4;
				}
				if (!this.props.checks[id](field.value)) {
					field.value = '';
					this.fields.delete(field);
				}
				else {
					this.updated[field.id] = field.value;
				}
			}
			if (Object.keys(this.updated).length > 0) {
				this.props.updateHandler(this.props.item.id, this.updated);
			}
		}
	}

	updateHandler(e) {
		if (e.target.value != '') {
			if (!this.fields.has(e.target))
				this.fields.add(e.target);
		}
		else {
			e.target.value = '';
			this.updated[e.target.id] = e.target.placeholder;
			if (this.fields.has(e.target))
				this.fields.delete(e.target);
		}
	}

	clearAll() {
		let fields = Array.from(this.fields);
		for (let field in fields) {
			fields[field].value = '';
			fields[field].placeholder = this.updated[fields[field].id];
		}
		this.updated = {};
		this.fields.clear();
	}

	handleFav(e) {
		if (this.props.item.fav == 'false') {
			this.props.favHandler(this.props.item, 'true');
		} else {
			this.props.favHandler(this.props.item, 'false');
		}
	}

	render() {
		let tile, title, row, footer, click, dblclick, disabled, lock, lockColor;
		switch (this.props.stage) {
			case 0: tile = 'tile';
					title = 'tile-title';
					row = 'tile-row';
					footer = 'tile-footer';
					click = this.onSelectHandler.bind(this);
					dblclick = this.onHighlightHandler.bind(this);
					disabled = 'disabled';
					lock = 'lock';
					lockColor = 'lock-icon'
					this.clearAll();
					break;

			case 1: tile = 'tile tile-clicked';
					title = 'tile-title tile-title-clicked';
					row = 'tile-row tile-row-clicked';
					footer = 'tile-footer tile-footer-clicked';
					click = this.onUnselectHandler.bind(this);
					disabled = 'disabled';
					lock = 'lock';
					lockColor = 'lock-icon-clicked';
					break;

			case 2: tile = 'tile tile-highlighted';
					title = 'tile-title tile-title-highlighted';
					row = 'tile-row tile-row-highlighted';
					footer = 'tile-footer tile-footer-highlighted';
					dblclick = this.onUnhighlightHandler.bind(this);
					disabled = '';
					lock = 'pencil';
					lockColor = 'lock-icon-highlighted';
					break;
		}

		return (
			<div className={tile}>
				<div onClick={click} className="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					<input onBlur={this.blurHandler.bind(this)} onChange={this.updateHandler.bind(this)} id='firstName' className={title} placeholder={this.props.item['firstName'].toUpperCase()} disabled={disabled}/>
				</div>
				<div onClick={this.handleFav.bind(this)} className={"col-lg-2 col-md-2 col-sm-2 col-xs-2 " + lockColor}>
					<span className={this.props.item.fav == 'true' ? 'glyphicon glyphicon-star' : 'glyphicon glyphicon-star-empty'} aria-hidden="true"></span>
				</div>
				<div onClick={dblclick} className={"col-lg-2 col-md-2 col-sm-2 col-xs-2 " + lockColor}>
					<span className={"glyphicon glyphicon-" + lock} aria-hidden="true"></span>
				</div>
				<div onClick={click} >
					<input onBlur={this.blurHandler.bind(this)} onChange={this.updateHandler.bind(this)} id='lastName' className={row} placeholder={this.props.item['lastName']} disabled={disabled}/>
				</div>
				<div onClick={click} >
					<input onBlur={this.blurHandler.bind(this)} onChange={this.updateHandler.bind(this)} id='email' className={row} placeholder={this.props.item['email']} disabled={disabled}/>
				</div>
				<div onClick={click} >
					<input onClick={click} onBlur={this.blurHandler.bind(this)} onChange={this.updateHandler.bind(this)} id='contactNumber' className={row} placeholder={this.props.item['contactNumber']} disabled={disabled}/>
				</div>
				<div onClick={click} >
					<input onClick={click} onBlur={this.blurHandler.bind(this)} onChange={this.updateHandler.bind(this)} id='dob' className={footer} placeholder={this.props.item['dob']} disabled={disabled}/>
				</div>
			</div>
		);
	}
}

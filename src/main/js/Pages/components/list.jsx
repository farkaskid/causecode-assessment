import React from 'react';
import ListItem from './listItem.jsx';

export default class List extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			modified: false,
		}
		this.selected = new Set();
		this.highlighted = new Set();
	}

	onHighlightHandler(tileData, value, tile) {
		let state = !this.state.modified;
		if (value == 2) {
			this.highlighted.add(tileData);
		}
		else {
			this.highlighted.delete(tileData);
		}
		if (!this.props.updateActive) {
			this.props.highlightHandler();
		}
		this.setState({ modified: state });
	}

	onSelectHandler(tileData, value, tile) {
		let state = !this.state.modified;
		if (value == 1) {
			this.selected.add(tileData);
		}
		else {
			this.selected.delete(tileData);
		}
		this.props.selectHandler(tileData, value, tile);
		this.setState({ modified: state });
	}

	updateHandler(id, data) {
		this.props.updateHandler(id, data);
	}

	favHandler(id, data) {
		this.props.favHandler(id, data);
		this.setState({ modified: !this.state.modified });
	}

	handleTab(e) {
		this.props.handleTab(e.target.parentNode.getAttribute('id'));
	}

	render() {
		let list1 = [], list = this.props.list.data[this.props.list.active], list2 = [];
		if (!this.props.updateActive)
			this.highlighted.clear();
		if (!this.props.deleteActive)
			this.selected.clear();
		for (let i = list.length-1; i >= 0; i--) {
			let stage = 0;
			if (this.selected.has(list[i].id))
				stage = 1;
			if (this.highlighted.has(list[i].id))
				stage = 2;
			list1.push(<div key={i} className="col-lg-4 col-md-4 col-sm-6 col-xs-12"><ListItem favHandler={this.favHandler.bind(this)} updateHandler={this.updateHandler.bind(this)} onSelectHandler={this.onSelectHandler.bind(this)} onHighlightHandler={this.onHighlightHandler.bind(this)} key={list[i].emailId} item={list[i]} stage={stage} checks={this.props.list.checks}/></div>);
		}

		for (let i = 0; i < this.props.list.label.length; i++) {
			list2.push(<li role="presentation" id={i.toString()} key={i} className={i == this.props.list.active ? 'active' : ''} onClick={this.handleTab.bind(this)}><a>{ this.props.list.label[i].length <= 10 ?  this.props.list.label[i] : `${this.props.list.label[i].slice(0, 10)}...` }</a></li>);
		}
		return (
			<div>
				<ul className="nav nav-tabs">
					{list2}
				</ul>
				<div className="row well">
					{list1}
				</div>
			</div>
		);
	}
}

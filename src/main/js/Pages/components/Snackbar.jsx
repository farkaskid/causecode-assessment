import React from 'react';

export default class SnackBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {changed: false};
    }

    render () {
        this.show = this.state.changed ? 'hide' : this.props.show;
        return (
            <div id={this.props.id} className={`snackbar ${this.show}`}>
                <div onClick={() => this.setState({changed: !this.state.changed})} className='pull-right'><span className="glyphicon glyphicon-remove"></span></div>
                {this.props.message}
            </div>
        );
    }
}

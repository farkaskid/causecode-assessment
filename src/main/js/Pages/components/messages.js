let messages = [
    'There are no contacts right now, click "Add Contact" to add a new contact',
    'Now you can see a blue tile, each tile will represent a single contact. A tile has three states, BLUE, RED and GREEN. Each stage exposes several options. Just click on the tile to make it RED',
    'This is the selection mode. Now the contact is considered as selected. Now you can delete it (from a group or from everywhere) and add it to an existing or new group. Add another contact to proceed further',
    'If a tile is BLUE, you can click on the lock on the tile to make it GREEN. You can toggle this Tutorial on/off at anytime by clicking the tutorial button',
    'This is the highlight mode. In this mode a tile is practically a form. You can type in any field to change its value, form validation still apply though, if you give numerical value in email field it will revert back to original value. Once you are done editing simply leave it, edit any other tiles if you want',
    'Once you are done editing tiles you can hit update button to update the contact permanently',
    'let\'s make a group. Select atleast one contact(click it!) and select "new group" option from "Add To Group" and give a fancy group name',
    'You can add contacts to an existing group in similar fashion. For removing contacts from this group, select them and select the "Contacts From Current Group" option from Delete menu',
    'Now let\'s search the contacts. First switch to the "All" tab then Search the contacts by selecting the field in the search bar and typing the value, it will start searching as soon as you start typing',
    'Search will be conducted in the contacts of current tab, so as we are in the "All" tab, we are searching in all the contacts. If you switch to some group\'s tab and search, the results will be based on that group',
    'You can click on the the little star on the tile to make it favourite. Basic Tutorial is complete here. Enjoy.'
]

export default messages;
